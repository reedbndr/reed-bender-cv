---
title: Reed Bender
permalink: /cv/
layout: default
---

# _Reed Bender_
_**Bioengineering, B.S.**_ <br>
_**Biomedical Data Science and Informatics, M.S.**_ <br><br>
_Generative AI Engineer_ // _Computational Biologist_ <br>
_last updated: Sep 13, 2023_ <br>
_[web format](https://reedbndr.gitlab.io/reed-bender-cv/cv/)_ <br>
_[PDF format](https://reedbndr.gitlab.io/reed-bender-cv/cv.pdf)_ 

[Email](mailto:reedbndr@gmail.com) / [Website](https://reedbender.com) / [LinkedIn](https://www.linkedin.com/in/reed-bender/) / [GitLab](https://gitlab.com/reedbndr) / [Twitter](https://twitter.com/reedbndr/)

---
## 👨🏼‍💻 Engineering Experience

### **Generative AI Engineer** @ [Praxis AI](https://praxis-ai.com/about/) _(September 2022 - Present)_

  - _**[Pria](https://praxis-ai.com/pria/)**_ -- AI Powered Virtual Mentor
    - Pria connects Langchain agents to GPT-4 as a research and education assistant. She combines custom literature search tools, Zapier web hooks, long-term vector embedding memory, image generation with Stable Diffusion, and compatibility with ChatGPT plugins.
    - I built the [Generative Intelligence Engine](https://reedbender.com/generative-intelligence-engine) while Praxis AI constructed the front-end integration.
      - **_[Try her here!](https://www.hiimpria.ai/signup)_** -- _free trial available_
    - Pria won the following awards at the 2023 American Buisness Awards:
      - *GOLD* for [Achievement in Online Training](https://stevieawards.com/aba/web-awards); _AI Teaching AI to High Schoolers_
      - *BRONZE* for [Use of Artificial Intelligence in EdTech](https://stevieawards.com/aba/product-management-new-product-awards); _Praxis AI Augmented Intelligence (A/I) Mentor Pria_
    - I personally engineered the Generative-AI backend and DevOps solutions while contributing to our production deployment and AWS infrastructure.

  - _**[Foundations of AI](https://praxis-ai.com/courses/course-ai-foundations/)**_ -- Digital Course
    - I created, taught, and mentored a 12 week course covering the following subject material:
      - Statistics, Data Science, Regression, Classification, Clustering, CNNs, RNNs, LSTMs, the transformer, and building with generative AI APIs.
    - This course won the following award at the 2023 American Buisness Awards:
      - *GOLD* for [K-12 Course or Learning Management Solution](https://stevieawards.com/aba/product-management-new-product-awards); _Praxis: AI Teaching AI to High Schoolers_

  - For further mention of these awards, see the Praxis AI press release _**[here](https://praxis-ai.com/press-release/2023-american-business-awards/)**_.
  - _[<span style="color: magenta;">LinkedIn Recommendation (1)</span>](https://reedbndr.gitlab.io/reed-bender-cv/resources/Feltus-recommendation.png)_
  <!-- - [LinkedIn Recommendation]({{ site.baseurl }}/resources/Feltus-recommendation.png) -->
  - _**Technologies Used:**_
    - OpenAI, Langchain, Stable Diffusion, Zapier, Python, Docker, Amazon Web Services, Flask, Gitlab C/I, Pinecone
<br>

### **Research Associate, Computational Biology / AI** @ [Flagship Labs 84](https://www.flagshippioneering.com/preemptive-health) _(September 2021 - March 2023)_
  - As the third employee of an early-stage pharmaceutical company aimed towards preventative-medicine, I built out our entire computational biology and cloud development software stack. As the first computational team member, I was directly responsible for the following:
    - Sys Admin for our teams's expanding AWS infrastructure;
    - Writing Python scripts to perform a variety of single cell biology analyses;
    - Implementing DevOps solutions to improve efficiency and collaboration in Gitlab;
    - Creating AWS compute environments and Nextflow pipelines for our workflows;
    - Managing all CUDA driver installations for GPU-enabled applications on AWS;
    - Communicating research progress to computational scientists, biologists, and stakeholders.
  - In February 2023, I switched to a contract role with FL84 to pursue my work with Praxis AI.
  - _[<span style="color: magenta;">LinkedIn Recommendation (2)</span>](https://reedbndr.gitlab.io/reed-bender-cv/resources/Lipnick_recommendation.png)_
  - _**Technologies used:**_ 
    - Python, Tensorflow, Keras, R Shiny, Amazon Web Services, Nextflow, Scanpy, Illumina 10x Sequencing, Gitlab, Notion
<br>

### **Consulting Research Scientist** @ [The Institute for Advanced Consciousness Studies (IACS)](https://advancedconsciousness.org/) _(August 2022 - Present)_
  - After attending [The Science of Consciousness](https://consciousness.arizona.edu/science-consciousness-conference) conference in April 2022, I began exploring the phenomenological effects of pineal gland neuromodulation with a team at IACS.
    - _[Research Proposal](https://advancedconsciousness.org/wp-content/uploads/2023/01/IACS-Pineal-Gland-Neuromodulation-Donation-Deck-January-2023.pdf)_
    - _[Crowdfunding Page](https://advancedconsciousness.org/donate/)_
  - I have also presented on generative AI applications to IACS, offering a journal club presentation covering the transformer model and its relation to human cognition.
    - _[Journal Club Presentation](https://reedbndr.gitlab.io/reed-bender-cv/resources/IACS_transformer_presentation.pdf)_
  - _[<span style="color: magenta;">LinkedIn Recommendation (3)</span>](https://reedbndr.gitlab.io/reed-bender-cv/resources/Reggente_recommendation.png)_

### **Product Mangement Intern** @ [DNAnexus](https://dnanexus.com/) _(May 2021 - August 2021)_
  - I wrote the initial documentation and spec sheet that became the [Transcriptomic Expression Quantification Workflow Walkthrough](https://documentation.dnanexus.com/science/scientific-guides/transcriptomic-expression-quantification-workflow-walkthrough). 
  - _**Technologies used:**_ 
    - Python, Confluence, Notion

### **Undergraduate --> Graduate Research Assistant** @ [Clemson University](https://www.clemson.edu/health-research/faculty/feltus.html) _(September 2018 - May 2021)_
  - I accomplished the following as an undergraduate and then graduate researcher in a computational biology lab:
    - Integrated a kidney cancer patient's RNA-seq data into a dataset of public consortium data (TCGA and GTEx);
    - Applied a novel deep learning algorithm to determine patient-specific gene expression abnormalities relative to healthy tissue;
    - Scaled various bioinformatics pipelines onto distributed Kubernetes compute environments and the Google Cloud Platform;
    - Trained new undergraduate members of the research lab on how to perform bioinformatics research.
  - _**Technologies used:**_ 
    - Python, Docker, Kubernetes, AWS, GCP, Tensorflow, Keras

---
## 🗞 Publication History

**Cellular State Transformations Using Deep Learning for Precision Medicine Applications.** Colin Targonski, M. Reed Bender, et al. _(September 2020)_ [Cell Patterns](https://doi.org/10.1016/j.patter.2020.100087). <br>

**Simulating the restoration of normal gene expression from different thyroid cancer stages using deep learning.** Nicole M. Nelligan, M. Reed Bender, F. Alex Feltus ._(June 2022)_ [BMC Cancer](https://doi.org/10.1186/s12885-022-09704-z). <br>

**EdgeCrafting: mining embedded, latent, nonlinear patterns to construct gene relationship networks.** Benafsh Husain, M. Reed Bender, Frank Alex Feltus. _(April 2022)_ [G3](https://doi.org/10.1093/g3journal/jkac042). <br>

**GEMmaker: process massive RNAseq datasets on heterogeneous computational infrastructure.** 
John A. Hadish, Tyler D. Biggs, Benjamin T. Shealy, M. Reed Bender, et al. _(May 2022)_ [BMC Bioinformatics](https://doi.org/10.1186/s12859-022-04629-7). <br>

**Exploring Lossy Compression of Gene Expression Matrices.** Colin Targonski, M. Reed Bender, et al. _(November 2019)_ [Supercomputing Conference](https://ieeexplore.ieee.org/abstract/document/8955120). <br>

**[Our Superconducting Consciousness](https://open.substack.com/pub/reedbender/p/our-superconducing-consciousness-26c9a82072fc?r=a4e3g&utm_campaign=post&utm_medium=web)**; _A synthesis of neurophysiology, alchemy, and yoga into a theory of transcendental experience._ _(February 2022)_
  - I propose a novel theory for endogenous super conduction through the lens of modern clinical research, experimental physics, and ancient esoteric ideas. <br>

---
## 🎙 Conversations
    
### The Mind Bender Podcast

- **David Field: The Fundamental Nature of Reality** _(February 6, 2023)_
<br>[The Mind Bender Podcast #1](https://youtu.be/qEuOFLzcaGc)<br>

### Appearances

- **Biohacking Turmeric** _(December 19, 2022)_
<br>[BioHackers Podcast Ep. 10](https://youtu.be/DWb190Ov774)<br>

- **Chaos Theory** _(June 21, 2022)_
<br>[BioHackers Podcast Ep. 2](https://youtu.be/k6g3l8ZI-VM)<br>

 ---
## 👀 Public Engagement

### Presentations

- **Motivations For and Perceptions of Participants Controlling Their Own Data** _(September 2019)_
<br>[NIH Cancer Moonshot Symposium on Patient Control of Genomic Data for Research and Health; Bethesda, MD](https://www.youtube.com/watch?v=uj_yCcmgTNI&feature=youtu.be)<br>

- **Accelerating Biological Research with Multicloud Kubernetes** _(September 2019)_
<br>[Cisco Container Platform Case Study](https://www.cisco.com/c/en/us/products/cloud-systems-management/container-platform/index.html#~case-study)<br>

### Hackathons

- **Biohacking Turmeric** _(March 2020)_
<br>_Praxis AI Biohackathon_; Digital Hackathon<br>
[The BioHackathon Process](https://praxis-ai.com/research/)<br>

- **Preclinical Drug Discovery,** _(March 2020)_
<br>_Tri-Con Drug Discovery Hackathon_; San Francisco, CA<br>
[2020 Kidney Cancer Hackathon -- Results](http://rarekidneycancer.org/blog/2020-kidney-cancer-hackathon-results)<br>

- **U-Hack-Med** _(May 2019)_
<br>_UT Southwestern's Lyda Hill Department of Bioinformatics_; Dallas, TX<br>
[The Isoform Potential of the Genome](https://www.u-hackmed.org/2019/team-6)<br>

---
## 🎓 Education

### Diplomas

**Biomedical Data Science and Informatics, M.S.** <br>
[Clemson University](https://www.clemson.edu/) -- Clemson, SC _(September 2019 - May 2021)_ <br>
  - Credits for this M.S. degree began during my fourth year of B.S. curriculum.

**Bioengineering, B.S.** <br>
[Clemson University](https://www.clemson.edu/) -- Clemson, SC _(September 2016 - May 2020)_ <br>
  - Biomaterials Concentration
  - Genetics Minor
  - Clemson Honors College

### Continued Education

**BioHacker: Cancer Transcriptomics** <br>
[Digital Credential](https://www.credly.com/badges/05616a55-f651-4f2d-9dd9-1868f12abde6?source=linked_in_profile) -- (March 2022)
  - Certification indicating a comprehensive understanding of bioinformatics tools to explore the cancer transcriptome. 

**Certified Yoga and Breathwork Instructor** <br>
[RYT-200 Accredation](https://www.yogaalliance.org/TeacherPublicProfile?tid=323710) -- (July 2021)
  - During my last year at Clemson while completing my M.S. degree, I went through a yoga teacher training curriculum at [Solshine Yoga](https://solshinewellness.com/). 

---
If you've taken the time to read through this CV, _thank you_! I hope to connect with you.

All the best, <br>
Reed

---


