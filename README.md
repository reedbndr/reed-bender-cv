# Reed Bender | 🧬🧠🤖

## Curriculum Vitae

Welcome to the repository of my personal CV, which is hosted on GitLab Pages.

You can view the CV [here](https://reedbndr.gitlab.io/reed-bender-cv/cv/)

---

### Social Media

Get in touch or follow me:

[![Twitter](https://img.shields.io/badge/-Twitter-blue?style=flat&logo=Twitter&logoColor=white)](https://twitter.com/reedbndr)
[![LinkedIn](https://img.shields.io/badge/-LinkedIn-blue?style=flat&logo=Linkedin&logoColor=white)](https://www.linkedin.com/in/reed-bender/)
[![GitLab](https://img.shields.io/badge/-GitLab-black?style=flat&logo=gitlab&logoColor=white)](https://gitlab.com/reedbndr/)

---

### Repository Information

[![pipeline status](https://gitlab.com/reedbndr/reed-bender-cv/badges/main/pipeline.svg)](https://gitlab.com/reedbndr/reed-bender-cv/commits/master)

---

### Local Hosting

For testing and development purposes, you can locally host this project by running the following command in your terminal:
```
bundle exec jekyll serve
```

After running this command, it is running locally at [http://127.0.0.1:4000/reed-bender-cv/cv/](http://127.0.0.1:4000/reed-bender-cv/cv/).
